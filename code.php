<?php

class Product{
	public $name;
	public $price;
	public $desc;
	public $category;
	public $stockNum;

	public function __construct($nameIn, $priceIn,$descIn,$stockNumIn,$categoryIn){ 
		$this->name =$nameIn ;
		$this->price =$priceIn ;
		$this->desc =$descIn ;
		$this->stockNum =$stockNumIn ;
		$this->category =$categoryIn ;
	}

	public function printDetails(){
		return "The product has a name of $this->name and its price is <productprice>, and the stock no is $this->stockNum";
	}
	public function getPrice(){
		return $this->price;
	}
	public function getStock(){
		return $this->stockNum;
	}
	public function getCategory(){
		return $this->category;
	}

	public function setPrice($priceIn){
		$this->price = $priceIn;
	}
	public function setStock($stockNumIn){
		$this->stock = $stockNumIn;
	}
	public function setCategory($categoryIn){
		$this->category = $categoryIn;
	}

}
class Mobile extends Product{
	public function printDetails(){
		return "Our latest mobile is $this->name with a cheapest price of $this->price";
	}

}
class Computer extends Product{
	public function printDetails(){
		return "Our newest computer is $this->name and its base price is $this->price";
	}

}
$newProduct = new Product("Xioami Mi Monitor", 22000.00, "Good for gaming and for coding", 5, "computer-peripherals");
$newMobile = new Mobile("Xioami Redmi Note 10 pro", 13590.00, "Latest Xioami phone ever made", 10, "mobiles and electronics");
$newComputer = new Computer("HP Business Laptop", 29000.00, "HP Laptop only made for business", 10, "laptops and computers");
?>

<!-- 			Product:
				name - Xioami Mi Monitor
				price - 22,000.00
				short description - Good for gaming and for coding
				stock no - 5
				category - computer-peripherals

			Mobile: 
				name - Xioami Redmi Note 10 pro
				price - 13590.00
				short description - Latest Xioami phone ever made
				stock no - 10
				category - mobiles and electronics

			Computer: 
				name - HP Business Laptop
				price - 29000.00
				short description - HP Laptop only made for business
				stock no - 10
				category - laptops and computer
 -->