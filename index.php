<?php require "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP | Activity</title>

</head>
<body>
	<ul>
		<li>
		
		<?php
		echo $newProduct->printDetails();
		?>
		</li>
		<li>
			
		<?php
		echo $newMobile->printDetails();
		?>
		</li>
		<li>
		<?php
		echo $newComputer->printDetails();
		?>
		</li>

	</ul>
	<pre>
		
	<?php 
	$newProduct->setStock(3);
	$newMobile->setStock(3);
	$newComputer->setCategory("laptops, computers and electronics");
	// echo "<br/> Updated Product stocks are: ".$newProduct->getStock();
	// echo "<br/> Updated Mobile stocks are: ".$newMobile->getStock();
	// echo "<br/> Updated Computer category is: ".$newComputer->getCategory();
		?>
	</pre>

</body>
</html>